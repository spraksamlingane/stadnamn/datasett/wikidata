import re
import time
from datetime import date
from urllib.error import HTTPError

from SPARQLWrapper import SPARQLWrapper, TSV, JSON

WD_ENDPOINT = 'https://query.wikidata.org/sparql'
WD_ITEM_REGEX = '^wd:Q[0-9]+$'
WD_NUMBER_OF_PLACE_QUERY = """SELECT (count(*) AS ?numberOfNorwegianPlaces)
        WHERE {{  ?s wdt:P131* {} . }}"""
WD_PLACE_QUERY = """
SELECT ?sted ?stedLabel
(GROUP_CONCAT(distinct ?demonym_ ;  SEPARATOR='||') AS ?demonym) 
(GROUP_CONCAT(distinct ?erstatter_ ; SEPARATOR='||') AS ?erstatter) 
(GROUP_CONCAT(distinct ?erstatterLabel ; SEPARATOR='||') AS ?erstatterTekst) 
(GROUP_CONCAT(distinct ?typeLabel ; SEPARATOR='||') AS ?stedstyper) 
(GROUP_CONCAT(distinct ?kommunenr_ ; SEPARATOR='||') AS ?kommunenr) 
(GROUP_CONCAT(distinct ?OSM_ ; SEPARATOR='||') AS ?OSM) 
(GROUP_CONCAT(distinct ?GN_ ; SEPARATOR='||') AS ?GN)
(GROUP_CONCAT(distinct ?SSR_ ; SEPARATOR='||') AS ?SSR) 
(GROUP_CONCAT(distinct ?SNL_ ; SEPARATOR='||') AS ?SNL) 
(GROUP_CONCAT(distinct ?bilde_ ; SEPARATOR='||') AS ?bilde) 
(GROUP_CONCAT(distinct ?overordnetSted_ ; SEPARATOR='||') AS ?overordnetSted) 
(GROUP_CONCAT(distinct ?land_ ; SEPARATOR='||') AS ?land) 
(GROUP_CONCAT(distinct ?geokoordinat_ ; SEPARATOR='||') AS ?geokoordinat) 
(GROUP_CONCAT(distinct ?lokaltNavn_ ; SEPARATOR='||') AS ?lokaltNavn) 
# Blazagraph specific syntax for "named subquery" - Takes precedence over optimized subqueries 
# (Blazegraph tries to inline statements as subqueries to optimize, which can stop our subqueries from precedence)
# https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/query_optimization#Named_subqueries
WITH  {{ SELECT distinct ?sted
WHERE  {{
          # P131 (is located in administrative)  Q20 (is Norway) 
          ?sted wdt:P131+ {wd_place}.         
          FILTER (?sted != {wd_place})    
           }}
  LIMIT {limit} OFFSET {offset}
    }} AS %steder           
WHERE {{            
      INCLUDE %steder
   OPTIONAL {{ ?sted  wdt:P31 ?type_. hint:Prior hint:rangeSafe true.   }}
   OPTIONAL {{ ?sted wdt:P1365 ?erstatter_ . hint:Prior hint:rangeSafe true.   }}
   SERVICE wikibase:label {{ bd:serviceParam wikibase:language "nb,no,nn,en". 
                            ?erstatter_ rdfs:label ?erstatterLabel.  hint:Prior hint:rangeSafe true.
                            ?sted rdfs:label ?stedLabel.  hint:Prior hint:rangeSafe true.
                            ?type_ rdfs:label ?typeLabel.  hint:Prior hint:rangeSafe true.
                          }}  
    OPTIONAL {{?sted wdt:P1549 ?demonym_ . hint:Prior hint:rangeSafe true. }} 
    OPTIONAL {{?sted wdt:P1705 ?lokaltNavn_ . hint:Prior hint:rangeSafe true.}}
    OPTIONAL {{?sted wdt:P6520 ?lokalhistoriewiki_. hint:Prior hint:rangeSafe true.  }}
    OPTIONAL {{?sted wdt:P4342 ?SNL_ . hint:Prior hint:rangeSafe true. }}
    OPTIONAL {{?sted wdt:P402 ?OSM_ . hint:Prior hint:rangeSafe true.  }}
    OPTIONAL {{?sted wdt:P1850 ?SSR_ . hint:Prior hint:rangeSafe true. }}
    OPTIONAL {{?sted wdt:P131+ ?kommune. hint:Prior hint:rangeSafe true. 
               ?kommune wdt:P2504 ?kommunenr_. hint:Prior hint:rangeSafe true.
             }}
    OPTIONAL {{ ?sted wdt:P18 ?bilde_ .  hint:Prior hint:rangeSafe true. }}  
    OPTIONAL {{ ?sted wdt:P1566 ?GN_ . hint:Prior hint:rangeSafe true}}
    OPTIONAL {{ ?sted wdt:P131 ?overordnetSted_ . hint:Prior hint:rangeSafe true.}}
    OPTIONAL {{?sted wdt:P17 ?land_ .  hint:Prior hint:rangeSafe true. }}
    OPTIONAL {{?sted wdt:P625 ?geokoordinat_ .  }}
}}
GROUP BY ?sted ?stedLabel
"""


def prepare_sparql_wrapper():
    wrapper = SPARQLWrapper(WD_ENDPOINT)
    # Custom header is required, WD endpoint only accepts format xml or json .
    # Force only conneg to return tsv
    wrapper.setOnlyConneg(onlyConneg=True)
    wrapper.addCustomHttpHeader("Accept", "text/tab-separated-values")
    # User agent condition for using WD service
    wrapper.addCustomHttpHeader("User-Agent",
                                "wikidata-no-tsv/0.0.1 (https://toponymi.spraksamlingane.no oyvind.gjesdal@uib.no)")
    wrapper.setReturnFormat(TSV)
    wrapper.setMethod("POST")
    return wrapper


def prepare_sparql_json_wrapper():
    wrapper = SPARQLWrapper(WD_ENDPOINT)
    wrapper.setReturnFormat(JSON)
    wrapper.setMethod("POST")
    return wrapper


def get_number_of_places_wd(wd_region_item: str):
    if not re.match(WD_ITEM_REGEX, wd_region_item):
        raise Exception("wd_region_item must match: " + WD_ITEM_REGEX)
    number_of_places_query = WD_NUMBER_OF_PLACE_QUERY.format(wd_region_item)
    wrapper = prepare_sparql_json_wrapper()
    wrapper.setQuery(number_of_places_query)
    total_no_places_result = wrapper.query().convert()
    return int(total_no_places_result["results"]["bindings"][0]["numberOfNorwegianPlaces"]["value"])


def get_place_names(total: int, limit=500, wd_place="wd:Q20"):
    offset = 0
    filename_out = "output/wikidata-no-places-" + str(date.today()) + ".tsv"

    #   overwrite file if exists
    f = open(filename_out, "wb")
    f.close()
    #   append/binary
    f = open(filename_out, "ab")
    wrapper = prepare_sparql_wrapper()

    while not offset > total + limit:
        query = WD_PLACE_QUERY.format(offset=offset, limit=limit, wd_place=wd_place)
        wrapper.setQuery(query)
        try:
            result = wrapper.query()
        except HTTPError:
            time.sleep(5)
            result = wrapper.query()
        print(str(offset) + " / " + str(total))
        for i, r in enumerate(result):
            # Skip header row for results after first file
            if i != 0 or offset == 0:
                f.write(r)
        offset += limit
    f.close()
    return


if __name__ == "__main__":
    wd_item_norway = "wd:Q20"
    get_place_names(total=get_number_of_places_wd(wd_item_norway),
                    limit=1000,
                    wd_place=wd_item_norway)
