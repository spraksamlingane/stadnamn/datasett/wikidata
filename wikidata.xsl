<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:saxon="http://saxon.sf.net/"
    xmlns:array="http://www.w3.org/2005/xpath-functions/array"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:param name="limit" select="400000"></xsl:param>
    <xsl:param name="query" select="'SELECT ?s
        WHERE {
        ?s wdt:P131* wd:Q20.  
        }'"></xsl:param>
    
    <xsl:variable name="start" select="saxon:timestamp()" as="xs:dateTimeStamp"/>
    <!-- force evaluation -->
    
    <xsl:variable name="wikidata-entity-url" select="'https://www.wikidata.org/wiki/Special:EntityData/'"/>
    <xsl:template name="xsl:initial-template">
        <xsl:if test="$start eq xs:dateTime('1900-01-01T00:00:00')"><x/></xsl:if>
        <xsl:variable name="query">
        <xsl:text expand-text="1">{encode-for-uri($query || 'LIMIT '||$limit)}</xsl:text>
        </xsl:variable>
        <xsl:message select="'https://query.wikidata.org/sparql?query='||$query"></xsl:message>
        <xsl:variable name="results" select="saxon:discard-document(document('https://query.wikidata.org/sparql?query='||$query))"/>
        <xsl:variable name="results-size" select="count($results//*:uri)"/>
       
        <xsl:for-each select="$results//*:uri" saxon:threads="5">            
                
            <xsl:variable name="wikidata-id" select="tokenize(.,'/')[last()]"/>             
                      <xsl:result-document href="output/{$wikidata-id}.rdf" method="xml">
                          <xsl:sequence select="saxon:discard-document(document($wikidata-entity-url || $wikidata-id || '.rdf?flavor=dump'))"/>  
                      </xsl:result-document>
        </xsl:for-each>
        
        <xsl:message expand-text="yes">Execution time: {(saxon:timestamp() - $start) div xs:duration('PT0.001S')}ms </xsl:message>
        <!--CONSTRUCT {?s ?p ?o}
        WHERE {
        VALUES (?s) {(<http://www.wikidata.org/entity/Q57305469>)
            (<http://www.wikidata.org/entity/Q11959695>)}
                ?s ?p ?o
                
                }-->
    </xsl:template>
    
    
</xsl:stylesheet>